package package11;

import java.util.ArrayList;
import java.util.List;

public class Zug {

	Lokomotive _lokomotive;
	List<Wagon> _wagons = new ArrayList<Wagon>();

	/**
	 * 
	 * Constructor of "Zug.java"
	 *
	 * @param lokomotive
	 */
	public Zug(Lokomotive lokomotive) {
		_lokomotive = lokomotive;
	}

	/**
	 * Haengt einen neuen Wagon an das Zugende heran.
	 * 
	 * @param wagon
	 */
	public void haengeAn(Wagon wagon) {

		if (_wagons.size() == 0) {
			_lokomotive.setzeNachfolger(wagon);
			_wagons.add(wagon);
		} else {
			Wagon letzterWagon = _wagons.get(_wagons.size() - 1);
			letzterWagon.setzeNachfolger(wagon);
			_wagons.add(wagon);
		}

	}

	/**
	 * Liefert die Zugbeschreibung aller Wagons als String zurück - Iterativ
	 * 
	 * @return
	 */
	public String gibZugbeschreibung1() {
		String result = _lokomotive.toString();
		for (Wagon wagon : _wagons) {
			result = result + " " + wagon.toString();
		}
		return result;

	}

	/**
	 * Liefert die Zugbeschreibung aller Wagons als String zurück - Rekursiv
	 * 
	 * @return
	 */
	public String gibZugbeschreibung2() {
		return _lokomotive.iteriereUeberZug(_lokomotive);
	}

}
