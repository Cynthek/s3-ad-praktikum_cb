package package11;

public class Personenwagon extends Wagon {

	private final int _sitzanzahl;

	/**
	 * 
	 * Constructor of "Personenwagon.java"
	 *
	 * @param bezeichnung
	 * @param sitzanzahl
	 */
	public Personenwagon(String bezeichnung, int sitzanzahl) {

		super(bezeichnung);
		_sitzanzahl = sitzanzahl;
	}

}
