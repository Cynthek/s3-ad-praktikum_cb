package package11;

public class Wagon {

	private final String _bezeichnung;
	private Wagon _naechsterWagon;

	/**
	 * 
	 * Constructor of "Wagon.java"
	 *
	 * @param bezeichnung
	 */
	public Wagon(String bezeichnung) {
		_bezeichnung = bezeichnung;
	}

	/**
	 * Gibt die Bezeichnung des aktuelle Wagons zurück.
	 */
	@Override
	public String toString() {
		return _bezeichnung;
	}

	/**
	 * Setzt einen Nachfolger für den aktuellen Wagon
	 * 
	 * @param wagon
	 */
	public void setzeNachfolger(Wagon wagon) {
		_naechsterWagon = wagon;
	}

	/**
	 * Gibt den Nachfolger des aktuellen Wagons zurück.
	 *
	 * @return
	 */
	public Wagon gibNachfolger() {
		return _naechsterWagon;

	}

}
