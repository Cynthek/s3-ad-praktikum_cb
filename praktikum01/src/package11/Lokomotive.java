package package11;

public class Lokomotive extends Wagon {

	private final int _zugkraft;

	/**
	 * 
	 * Constructor of "Lokomotive.java"
	 *
	 * @param bezeichnung
	 * @param zugkraft
	 */
	public Lokomotive(String bezeichnung, int zugkraft) {

		super(bezeichnung);
		_zugkraft = zugkraft;

	}

	/**
	 * Iteriert über alle Elemente des Zuges und gibt diese als String zurück.
	 * 
	 * @param wagon
	 * @return
	 */
	public String iteriereUeberZug(Wagon wagon) {

		if (wagon.gibNachfolger() != null) {
			return wagon.toString() + " " + iteriereUeberZug(wagon.gibNachfolger());
		} else {
			return "";
		}

	}

}
