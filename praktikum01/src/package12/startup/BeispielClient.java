package package12.startup;

import package12.Lokomotive;
import package12.Personenwagon;
import package12.Zug;

public class BeispielClient {

	static Zug zug;
	static Lokomotive lokomotive;

	public static void main(String[] args) {

		generiereZugMitLaenge(10);
		System.out.println(zug.gibZugbeschreibung());
		generiereZugMitLaenge(15);
		System.out.println(zug.gibZugbeschreibung());
	}

	/**
	 * Generiert einen Zug mit gewünschter Laenge
	 *
	 * @param anzahl
	 */
	private static void generiereZugMitLaenge(int anzahl) {

		lokomotive = new Lokomotive("ICE", 5);
		zug = new Zug(lokomotive);

		for (int i = 0; i < anzahl; i++) {
			zug.haengeAn(new Personenwagon("Wagon" + i, 10));
		}
	}

}
