package package12;

import java.util.ArrayList;
import java.util.List;

public class Zug {

	Lokomotive _lokomotive;
	List<Wagon> _wagons = new ArrayList<Wagon>();

	/**
	 * 
	 * Constructor of "Zug.java"
	 *
	 * @param lokomotive
	 */
	public Zug(Lokomotive lokomotive) {
		_lokomotive = lokomotive;
	}

	/**
	 * Haengt einen neuen Wagon an das Zugende heran.
	 * 
	 * @param wagon
	 */
	public void haengeAn(Wagon wagon) {

		_wagons.add(wagon);

	}

	/**
	 * Liefert die Zugbeschreibung aller Wagons als String zurück - Iterativ
	 * 
	 * @return
	 */
	public String gibZugbeschreibung() {
		String result = _lokomotive.toString();
		for (Wagon wagon : _wagons) {
			result = result + " " + wagon.toString();
		}
		return result;

	}

}
