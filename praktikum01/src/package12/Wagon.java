package package12;

public class Wagon {

	private final String _bezeichnung;

	/**
	 * 
	 * Constructor of "Wagon.java"
	 *
	 * @param bezeichnung
	 */
	public Wagon(String bezeichnung) {
		_bezeichnung = bezeichnung;
	}

	/**
	 * Gibt die Bezeichnung des aktuelle Wagons zurück.
	 */
	@Override
	public String toString() {
		return _bezeichnung;
	}

}
