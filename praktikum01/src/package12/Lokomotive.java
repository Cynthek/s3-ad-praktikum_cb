package package12;

public class Lokomotive extends Wagon {

	private final int _zugkraft;

	/**
	 * 
	 * Constructor of "Lokomotive.java"
	 *
	 * @param bezeichnung
	 * @param zugkraft
	 */
	public Lokomotive(String bezeichnung, int zugkraft) {

		super(bezeichnung);
		_zugkraft = zugkraft;

	}

}
