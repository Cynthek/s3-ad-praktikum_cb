package package13.startup;

import package13.stack.ADStack;
import package13.stack.KnotenStack;

public class BeispielClient {

	public static void main(String[] args) {

		ADStack stack = new KnotenStack();
				
		stack.push(10);
		stack.push(11);
		stack.push(12);
		stack.push(13);
		stack.push(14);
		stack.push(15);
		stack.push(14);
		stack.push(10);
		stack.push(11);
		stack.push(12);
		stack.push(13);
		stack.push(14);
		stack.push(15);
		stack.push(14);
	
		System.out.println(stack.getStringRepresentation());
	}
}
