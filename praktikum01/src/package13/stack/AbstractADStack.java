package package13.stack;

public abstract class AbstractADStack implements ADStack {

	protected int _size = 0;

	/**
	 * 
	 * {Description of "push"}
	 *
	 * TODO: [28.09.2016 / 23:41:39, Christian] Finish the method "push" in
	 * "AbstractADStack.java"
	 * 
	 * @param o
	 * @ensure o != null;
	 */
	public void push(Object o) {
		assert o != null : "Fehler: Das Objekt ist leer!";
		pushImpl(o);
		_size++;
	}

	/**
	 * 
	 * @ensure return != null;
	 */
	public Object top() {
		return topImpl();

	}

	/**
	 * 
	 */
	public void pop() {
		assert _size >= 0 : "Fehler: Der Stack ist leer";

		if (_size > 0) {
			popImpl();
			_size--;
		}
	}

	/**
	 * 
	 */
	public int size() {
		return _size;
	}

	protected abstract void pushImpl(Object o);

	protected abstract Object topImpl();

	protected abstract void popImpl();

}
