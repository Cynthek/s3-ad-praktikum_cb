package package13.stack;

public class KnotenStack extends AbstractADStack {

	Knoten _startKnoten = null;

	public KnotenStack() {
		_startKnoten = new Knoten();
	}

	@Override
	public String getStringRepresentation() {
		String result = "";
		Knoten aktuellerKnoten = _startKnoten;
		while (aktuellerKnoten.hatNachfolger()) {
			if (aktuellerKnoten.gibNachfolger() != null) {
				result = result + aktuellerKnoten.toString() + "\n";
				aktuellerKnoten = (Knoten) aktuellerKnoten.gibNachfolger();
			} else {
				result = result + aktuellerKnoten.toString() + "\n";
			}
		}
		return result;
	}

	@Override
	protected void pushImpl(Object o) {
		if (_size == 0) {
			_startKnoten.setzeWert(o);
			_startKnoten.setzeNachfolger(new Knoten());
		} else {

			Knoten aktuellerKnoten = _startKnoten;

			while (aktuellerKnoten.hatNachfolger()) {
				aktuellerKnoten = (Knoten) aktuellerKnoten.gibNachfolger();
			}

			aktuellerKnoten.setzeWert(o);
			aktuellerKnoten.setzeNachfolger(new Knoten());
		}
	}

	@Override
	protected Object topImpl() {
		Knoten aktuellerKnoten = _startKnoten;
		for (int i = 0; i < _size - 1; i++) {
			aktuellerKnoten = (Knoten) aktuellerKnoten.gibNachfolger();
		}

		return aktuellerKnoten.gibWert();
	}

	@Override
	protected void popImpl() {
		if (_size == 0) {
			_startKnoten.setzeWert(null);
		} else {

			Knoten aktuellerKnoten = _startKnoten;

			for (int i = 0; i < _size - 1; i++) {
				aktuellerKnoten = (Knoten) aktuellerKnoten.gibNachfolger();
			}

			aktuellerKnoten.setzeNachfolger(null);
		}
	}

}
