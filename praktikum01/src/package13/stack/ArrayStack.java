package package13.stack;

public class ArrayStack extends AbstractADStack {

	Object[] _stack;

	public ArrayStack() {
		_stack = new Object[5];
	}

	@Override
	public String getStringRepresentation() {
		String result = "";

		for (int i = 0; i < _size; i++) {
			result = result + " " + _stack[i].toString();
		}
		return result;
	}

	@Override
	protected void pushImpl(Object o) {
		this.pruefeKapazitaet();
		_stack[_size] = o;

	}

	@Override
	protected Object topImpl() {
		return _stack[_size - 1];
	}

	@Override
	protected void popImpl() {
		_stack[_size - 1] = null;
	}

	public int getKapazitaet() {
		return _stack.length;
	}

	private void pruefeKapazitaet() {
		if (_size == _stack.length) {
			Object[] neuerStack = new Object[_size * 2];

			for (int i = 0; i < _stack.length; i++) {
				neuerStack[i] = _stack[i];
			}

			_stack = neuerStack;
			System.out.println("Neuen Stack erzeugt mit Kapazität " + getKapazitaet());
		}
	}
}
