package package13.stack;

public class Knoten {

	Knoten _nachfolger = null;
	Object _object = null;

	public void setzeWert(Object o) {
		_object = o;
	}

	public Object gibWert() {
		return _object;
	}

	public void setzeNachfolger(Knoten knoten) {
		_nachfolger = knoten;
	}

	public Object gibNachfolger() {
		return _nachfolger;
	}

	public boolean hatNachfolger() {
		if (_nachfolger == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public String toString() {
		return _object.toString();

	}

}
