package package13.stack;

public interface ADStack {

	/**
	 * 
	 * {Description of "push"}
	 *
	 * Eine Verändernde Operation, legt das gegebene Objekt auf den Stack.
	 * 
	 * @param o
	 */
	public void push(Object o);

	/**
	 * 
	 * Gibt das zuletzt auf den Stack gelegte Objekt zurück.
	 * 
	 * @return
	 */
	public Object top();

	/**
	 * 
	 * Löscht das zuletzt auf den Stack gelegte Objekt.
	 */
	public void pop();

	/**
	 * 
	 * Liefert die Größe des aktuellen Stacks als integer zurück.
	 */
	public int size();

	/**
	 * 
	 * Liefert eine String-Repräsenstation des gesamten Stacks
	 * 
	 * @return
	 */
	public String getStringRepresentation();

}
